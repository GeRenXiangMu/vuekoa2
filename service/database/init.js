const mongoose=require('mongoose')
const db="mongodb://localhost/smile-vue" //可以不用写端口号默认就是27017
const glob=require('glob')  // 需要安装
const {resolve}=require('path')

//初始化映射
exports.initSchems=()=>{
   glob.sync(resolve(__dirname,'./schema','**/*.js')).forEach(require)
}

exports.connect=()=>{
   //链接数据库
    mongoose.connect(db)
    let maxConnectTimes=0;


   return new Promise((resolve ,reject)=>{
           //增加数据库监听事件
           mongoose.connection.on('disconnected',()=>{
             console.log("*********************数据库断开*******************");
             if(maxConnectTimes<=3){
               maxConnectTimes++
               mongoose.connect(db)
             }else{
               reject()
               throw new Error('数据库出现问题，程序无法搞定，请人为处理.....')
             }

           })
           /*数据库出错的时候重连*/
           mongoose.connection.on('error',(err)=>{
             console.log("*********************数据库错误******");
             if(maxConnectTimes<=3){
               maxConnectTimes++
               mongoose.connect(db)
             }else{
               reject(err)
               throw new Error('数据库出现问题，程序无法搞定，请人为处理.....')
             }
           })
           /*数据库链接打开时*/
           mongoose.connection.once('open',()=>{
             console.log("*********************数据库connected successfuly******");

             resolve()
           })
   })


}
