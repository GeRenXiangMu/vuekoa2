const Koa=require('koa')
const app=new Koa()
const {connect,initSchems}=require('./database/init.js')
const mongoose=require('mongoose')

const bodyParser=require('koa-bodyparser')
const cors=require('koa2-cors')  //解决跨域问题的组件

  const Router=require('koa-router')

app.use(bodyParser())
app.use(cors())  //解决跨域问题的组件


  let user=require('./appApi/user.js')
  let home=require('./appApi/home.js')
  let goods=require('./appApi/goods.js')

    //装载所有子路由
    let router=new Router()
      router.use('/user',user.routes())
      router.use('/home',home.routes())
      router.use('/goods',goods.routes())
    //加载路由中间件
     app.use(router.routes())
     app.use(router.allowedMethods())

;(async()=>{
  await connect()
  initSchems()
  /*const  User=mongoose.model('User')

  let oneUser=new User({userName:'jspangaaabbbb1223j3',password:'123456789'});
  oneUser.save().then(()=>{
       console.log('插入成功!')
  })
  let user=await User.findOne({}).exec();
  console.log('________________________________')
  console.log(user)
  console.log('________________________________')*/

})()
app.use(async(ctx)=>{
      ctx.body='<h1>hello koa2</h1>'
})

app.listen(3000,()=>{
   console.log('[server] starting at port 3000')
})
