import Vue from 'vue'
import Router from 'vue-router'
import ShoppingMail from '@/components/pages/ShoppingMail'
import Register from '@/components/pages/Register'
import Login from '@/components/pages/Login'
import Goods from '@/components/pages/goods'
import CategoryList from '@/components/pages/CategoryList'

import Cart from '@/components/pages/Cart'
import Main from '@/components/pages/Main'
Vue.use(Router)
export default new Router({
  routes: [
    /*主页*/
    {
      path: '/main',
      name: 'Main',
      component: Main,
       children:[
         { path: '/shoppingMail', name: 'ShoppingMail', component: ShoppingMail},//首页
         {path: '/categoryList', name: 'CategoryList', component: CategoryList},
         {path: '/cart', name: 'Cart', component: Cart}/*购物车*/
       ]
    },
    {path: '/register', name: 'Register', component: Register},
    {path: '/login', name: 'Login', component: Login},
    {path: '/goods', name: 'Goods', component: Goods},
  ]
})
